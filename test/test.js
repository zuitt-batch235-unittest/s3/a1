const chai = require("chai");
const { assert } = require("chai");

//import and use chai-http to allow chai to send request to our server
const http = require("chai-http");
chai.use(http);

describe("API Test Suite for users", () => {
  it("Test API get users is running", (done) => {
    //request()method is used from chai to create an http request give to the server
    chai
      .request("http://localhost:4000")
      //.get("/endpoint")methodis used to run/access a get method route
      .get("/users")
      //end((err, res))method is used to access the response from the route. It has an annonymouse function as an argument that receives 2 objects, the err or the response.
      .end((err, res) => {
        //isDefined is assertion that given data is not undefined. It's like a shortcut to .notEqual(typeof data, undefined)
        assert.isDefined(res);
        //done()method is used to tell chai-http when the test is done.
        done();
      });
  });
  it("Test AP get users returns an array", (done) => {
    chai
      .request("http://localhost:4000")
      .get("/users")
      .end((err, res) => {
        assert.isArray(res.body);
        done();
      });
  });
  it("Test API get users array first object usernmae is Jojo", (done) => {
    chai
      .request("http://localhost:4000")
      .get("/users")
      .end((err, res) => {
        assert.equal(res.body[0].username, "Jojo");
        done();
      });
  });
  it("Test API get users last object is not undefined", (done) => {
    chai
      .request("http://localhost:4000")
      .get("/users")
      .end((err, res) => {
        assert.equal(res.body[res.body.lenght - 1], undefined);
        done();
      });
  });

  it("Test API post users returns 400 if no name", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/users")
      .type("json")
      .send({
        age: 30,
        username: "jin92",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });
  it("Test API post users returns 400 if no age", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/users")
      .type("json")
      .send({
        name: "Jin",
        username: "jin92",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });
});

// ACTIVITY

describe("API Test for artists", () => {
  it("Test get artists method is running", (done) => {
    chai
      .request("http://localhost:4000")
      .get("/artists")
      .end((err, res) => {
        assert.isDefined(res);
        done();
      });
  });

  it("Test get artists array first object's songs is an array", (done) => {
    chai
      .request("http://localhost:4000")
      .get("/artists")
      .end((err, res) => {
        assert.isArray(res.body[0].songs);
        done();
      });
  });

  it("Test post artists returns 400 if no name", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/artists")
      .type("json")
      .send({
        songs: ["Test Artist 1 Song 1", "Test Artist 1 Song 2"],
        album: "Test Artist 1 Album 1",
        isActive: true,
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("Test post artists returns 400 if no songs", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/artists")
      .type("json")
      .send({
        name: "Test Artist 1",
        album: "Test Artist 1 Album 1",
        isActive: true,
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("Test post artists returns 400 if no album", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/artists")
      .type("json")
      .send({
        name: "Test Artist 1",
        songs: ["Test Artist 1 Song 1", "Test Artist 1 Song 2"],
        isActive: true,
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("Test post artists returns 400 if isActive status is false", (done) => {
    chai
      .request("http://localhost:4000")
      .post("/artists")
      .type("json")
      .send({
        name: "Test Artist 1",
        songs: ["Test Artist 1 Song 1", "Test Artist 1 Song 2"],
        album: "Test Artist 1 Album 1",
        isActive: false,
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });
});
