const express = require("express");
const app = express();
const PORT = 4000;

app.use(express.json());

let users = [
  {
    name: "Jojo Joestar",
    age: 25,
    username: "Jojo",
  },
  {
    name: "Dio Brando",
    age: 23,
    username: "Dio",
  },
  {
    name: "Jotaro Kujo",
    age: 20,
    username: "Jotaro",
  },
];

app.get("/users", (req, res) => {
  console.log(users);
  return res.send(users);
});

app.post("/users", (req, res) => {
  //add simple if statement that if the request body does noyt have property name, we will send a message along with a 400 http status code(Bad reques)
  //hasOwnproperty erturns a boolean if the property name passed exists or doesnot exist in the given object.
  if (!req.body.hasOwnProperty("name")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter NAME",
    });
  }
  if (!req.body.hasOwnProperty("age")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter NAME",
    });
  }
});

// ACTIVITY

let artists = [
  {
    name: "Artist 1",
    songs: ["Artist 1 Song 1", "Artist 1 Song 2"],
    album: "Artist 1 Album 1",
    isActive: true,
  },
  {
    name: "Artist 2",
    songs: ["Artist 2 Song 1", "Artist 2 Song 2"],
    album: "Artist 2 Album 1",
    isActive: true,
  },
  {
    name: "Artist 3",
    songs: ["Artist 3 Song 1", "Artist 3 Song 2"],
    album: "Artist 3 Album 1",
    isActive: true,
  },
];

app.get("/artists", (req, res) => {
  return res.send(artists);
});

app.post("/artists", (req, res) => {
  if (!req.body.hasOwnProperty("name")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter Name",
    });
  }

  if (!req.body.hasOwnProperty("songs")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter Songs",
    });
  }

  if (!req.body.hasOwnProperty("album")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter Album",
    });
  }

  if (!req.body.hasOwnProperty("isActive")) {
    return res.status(400).send({
      error: "Bad Request - missing required parameter isActive",
    });
  }

  if (req.body.isActive === false) {
    return res.status(400).send({
      error: "Bad Request - user isActive status is false",
    });
  }
});

app.listen(PORT, () => console.log(`Running on port ${PORT}`));
